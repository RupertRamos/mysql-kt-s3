--Add the following records to the blog_db database: FOR USERS TABLE

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacreuz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

--Add the following records to the blog_db database: FOR POSTS TABLE

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Fourth Code", "Bye bye Solar System!", "2021-01-02 04:00:00");


--Get all the post with an Author ID of 1 (User ID).
SELECT * FROM posts WHERE user_id = 1;

-- Get all the user's email and datetime_created.
SELECT email, datetime_created FROM users;

--Update a post's content to "Hello to the people of the Earth!" where it's initial content is "Hello Earth!" by using the record's ID.
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!" AND id = 2; 

--Delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";

